package com.example;

import java.sql.PseudoColumnUsage;

public class TimeCalculator {
    /*Write a simple program to create a Time class where the values like seconds, minutes, day, etc
     are collected and displayed. Create Time as local Inner class and display the output on screen.
     */
     int seconds;
    int minutes;
     int hours;
     int days;

    public TimeCalculator(int seconds) {
        this.seconds = seconds;
        this.minutes=seconds/60;
        this.hours=minutes/60;
        this.days=hours/24;
    }
     public class Time{
       public void display(){
           System.out.println("Seconds "+seconds);
           System.out.println("Minutes "+minutes);
           System.out.println("Hours "+hours);
           System.out.println("Days "+days);
       }
    }
}
